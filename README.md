[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cbenz%2Ftest-bqplot-waterfall/master?urlpath=lab)

## Install

```bash
pip install -r requirements.txt
./postBuild
```

## Usage

```bash
jupyter lab
```

Then open the notebooks.
